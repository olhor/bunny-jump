import Phaser from './lib/phaser.js'
import Game from './scenes/game.js'
import GameOver from './scenes/game_over.js'
import Welcome from './scenes/welcome.js'

export default new Phaser.Game({
  type: Phaser.AUTO,
  width: 480,
  height: 640,
  scene: [Welcome, Game, GameOver],
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 200
      },
      debug: false
    }
  }
})
