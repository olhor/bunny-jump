import Phaser from '../lib/phaser.js'

export default class GameOver extends Phaser.Scene {
  collectedCarrots

  constructor() {
    super('game-over')
  }

  init(data) {
    this.collectedCarrots = data.carrots
  }

  create() {
    const width = this.scale.width
    const height = this.scale.height

    this.add.text(width * 0.5, height * 0.5, 'Koniec gry', { fontSize: 48 })
      .setOrigin(0.5)
    this.add.text(width * 0.5, height * 0.6, `Udało Ci się zebrać ${this.carrotText()}`, { fontSize: 24 })
      .setOrigin(0.5)
    this.add.text(width * 0.5, height * 0.7, 'Wciśnij spację, aby rozpocząć', { fontSize: 24 })
      .setOrigin(0.5)

    this.input.keyboard.once('keydown_SPACE', () => {
      this.scene.start('game')
    })
  }

  carrotText() {
    let tmp = `${this.collectedCarrots}`
    if(this.collectedCarrots === 0 || this.collectedCarrots >= 5) {
      tmp += ' marchewek'
    } else if(this.collectedCarrots === 1) {
      tmp += ' marchewkę'
    } else if(this.collectedCarrots >= 2 && this.collectedCarrots < 5) {
      tmp += ' marchewki'
    }
    return tmp
  }
}
