import Phase from '../lib/phaser.js'
import Carrot from "../game/carrot.js";

/**
 * Main game scene.
 */
export default class Game extends Phaser.Scene {
  carrotsCollected
  carrotsCollectedText
  debugLog
  platforms
  player
  cursors
  carrots

  constructor() {
    super('game')
  }

  init() {
    this.carrotsCollected = 0
  }

  preload() {
    this.load.image('background', 'assets/bg_layer1.png') // load background image
    this.load.image('platform', 'assets/ground_grass.png') // load platform image
    this.load.image('bunny-stand', 'assets/bunny1_stand.png') // load player (stand) image
    this.load.image('bunny-jump', 'assets/bunny1_jump.png') // load player (jump) image
    this.load.image('carrot', 'assets/carrot.png')

    this.load.audio('jump', 'assets/sfx/phaseJump1.ogg')
    this.load.audio('eat', 'assets/sfx/powerUp2.ogg')

    this.cursors = this.input.keyboard.createCursorKeys() // add input
  }

  create() {
    this.add.image(240, 320, 'background') // image origin is in center of it
      .setScrollFactor(1, 0) // do not scroll vertically background when camera moves
    // this.physics.add.image(240, 320, 'platform').setScale(0.5) // adds platform with physics enabled

    // add group of platforms to the scene
    this.platforms = this.physics.add.staticGroup()

    for (let i = 0; i < 5; i++) {
      const x = Phaser.Math.Between(80, 400)
      const y = 150 * i
      const platform = this.platforms.create(x, y, 'platform')
      platform.scale = 0.5
      const body = platform.body
      body.updateFromGameObject()
    }

    // add player to the scene
    this.player = this.physics.add.sprite(240, 320, 'bunny-stand')
      .setScale(0.5)
    this.physics.add.collider(this.platforms, this.player)

    // change player's collision
    this.player.body.checkCollision.up = false
    this.player.body.checkCollision.left = false
    this.player.body.checkCollision.right = false

    // add following camera
    this.cameras.main.startFollow(this.player)
    // set horizontal dead zone to 1.5x game width, dead zone is area around player where camera cannot scroll
    this.cameras.main.setDeadzone(this.scale.width * 1.5)

    // create carrots with physics!
    // const carrot = new Carrot(this, 240, 320, 'carrot')
    // this.add.existing(carrot)
    this.carrots = this.physics.add.group({
      classType: Carrot
    })
    // this.carrots.get(240, 320, 'carrot') // gets existing carrot from group or creates new one
    this.physics.add.collider(this.platforms, this.carrots)

    // add overlap operation between player and carrot in order to collect them
    this.physics.add.overlap(this.player, this.carrots, null, this.handleCollectCarrot, this)

    // add text UI
    const style = { color: '#000', fontsize: 24}
    this.carrotsCollectedText = this.add.text(240, 10, 'Zebrane marchewki: 0', style)
      .setScrollFactor(0)
      .setOrigin(0.5, 0)

    // add debug log
    this.debugLog = {}
    this.debugLog.playerY = this.player.y
    this.debugLog.cameraY = this.cameras.main.scrollY
  }

  update(time, dt) {
    let cameraY = this.cameras.main.scrollY // top of camera
    // console.log(cameraY)
    // moves existing platforms already scrolled out of camera view to top of camera view
    this.platforms.children.iterate(platform => {
      if(platform.y >= cameraY + 700) {
        platform.y = cameraY - Phaser.Math.Between(50, 75)
        platform.x = Phaser.Math.Between(80, 400)
        platform.body.updateFromGameObject()

        // create a carrot above moved platform
        this.addCarrotAbove(platform)
      }
    })

    // remove not collected carrots
    this.removeNotCollectedCarrots()

    // find out from Arcade Physics if the player's physics body is touching something below it
    const touchingDown = this.player.body.touching.down
    if(touchingDown) {
      // this makes the bunny jump straight up
      this.player.setVelocityY(-300)
      this.player.setTexture('bunny-jump') // switch to jump texture
      this.sound.play('jump') // play jump sound
      this.player.body.setSize(this.player.width, this.player.height)
    }

    // change to standing texture when falling
    const vy = this.player.body.velocity.y
    if (vy > 0 && this.player.texture.key !== 'bunny-stand') {
      this.player.setTexture('bunny-stand')
      this.player.body.setSize(this.player.width, this.player.height)
    }

    // left and right input logic
    if(this.cursors.left.isDown && !touchingDown) {
      this.player.setVelocityX(-200)
    } else if(this.cursors.right.isDown && !touchingDown) {
      this.player.setVelocityX(200)
    } else {
      // stop horizontal movement if not left or right
      this.player.setVelocityX(0)
    }

    // game over rules
    const bottomPlatform = this.findBottomMostPlatform()
    if(this.player.y > bottomPlatform.y + 200) {
      this.scene.start('game-over', { carrots: this.carrotsCollected })
    }

    // check game sides wrapping the player
    this.horizontalWrap(this.player)
    this.verticalWrap(cameraY)
  }

  /**
   * Used to wrap any sprite around scene borders. Wrapped sprite 'teleports' to opposite border.
   * @param {Phaser.GameObjects.Sprite} sprite
   */
  horizontalWrap(sprite) {
    const halfWidth = sprite.displayWidth * 0.5
    const gameWidth = this.scale.width
    if(sprite.x < -halfWidth) {
      sprite.x = gameWidth + halfWidth
    } else if(sprite.x > gameWidth + halfWidth) {
      sprite.x = -halfWidth
    }
  }

  // prevents camera's vertical counter reaching infinity by wrapping world to game bounds
  verticalWrap(cameraY) {
    if(cameraY < -1000) { // map scrolled out of bounds
      this.platforms.children.iterate(platform => {
        platform.y += 1000
        platform.body.updateFromGameObject()
      })
      this.carrots.children.iterate(carrot => {
        carrot.y += 1000
      })
      this.player.y += 1000
    }
  }

  /**
   * Adds carrot above other sprite.
   * @param {Phaser.GameObjects.Sprite} sprite
   */
  addCarrotAbove(sprite) {
    const y = sprite.y - sprite.displayHeight
    const carrot = this.carrots.get(sprite.x, y, 'carrot')
    //set active and existing
    carrot.setActive(true)
    carrot.setVisible(true)

    this.add.existing(carrot)
    // update the physics body size
    // carrot.body.setSize(carrot.width, carrot.height)
    // enable its body in physics world
    this.physics.world.enable(carrot)
    carrot.body.checkCollision.up = false
    carrot.body.checkCollision.left = false
    carrot.body.checkCollision.right = false

    return carrot
  }

  /**
   * Called when physics overlap occurs.
   */
  handleCollectCarrot(player, carrot) {
    // hide from display
    this.carrots.killAndHide(carrot)
    // disable from physics world
    this.physics.world.disableBody(carrot.body)
    // increment by 1
    this.carrotsCollected++
    this.carrotsCollectedText.text = `Zebrane marchewki: ${this.carrotsCollected}`
    this.sound.play('eat') // play eat carrot sound
    return true
  }

  findBottomMostPlatform() {
    const platforms = this.platforms.getChildren()
    let bottomPlatform = platforms[0]
    for(let i = 1; i < platforms.length; i++) {
      if(platforms[i].y > bottomPlatform.y) {
        bottomPlatform = platforms[i]
      }
    }
    return bottomPlatform
  }

  removeNotCollectedCarrots() {
    const carrots = this.carrots.getChildren()
    const cameraY = this.cameras.main.scrollY // top of camera
    for(let i = 0; i < carrots.length; i++) {
      let carrot = carrots[i]
      if(carrot.y >= cameraY + 700) {
        this.carrots.killAndHide(carrot)
        this.physics.world.disableBody(carrot.body)
      }
    }
  }
}
